<?php
/**
 * @file
 * OG Capr library.
 */

/**
 * OgCapr library.
 */
class OgCapr {
  /**
   * OG roles labels.
   *
   * @var mixed
   */
  private static $ridLabels = array();
  /**
   * Groups labels.
   *
   * @var mixed
   */
  private static $gidLabels = array();
  /**
   * Node grants values.
   *
   * @var mixed
   */
  private static $nodeGrants = NULL;
  /**
   * Node access values.
   *
   * @var mixed
   */
  private static $nodeAccess = NULL;
  /**
   * Return all allowed OG Roles required to see a defined node.
   *
   * @param int $nid
   *    The node id.
   * @param array $gids
   *    The associated groups for this node.
   *
   * @return array
   *    An array of allowed role permissions.
   */
  public static function getAllowedOgRole($nid, array $gids = array()) {
    $query = db_select('nodeAccess', 'N')
      ->fields('N', array('gid'))
      ->condition('N.nid', $nid);
    if (!empty($gids)) {
      $or = db_or();
      foreach ($gids as $gid) {
        $or->condition('N.realm', OG_CAPR_REALM . ':' . $gid, '=');
      }
      $query->condition($or);
    }
    return $query->execute()->fetchAll(PDO::FETCH_COLUMN);
  }
  /**
   * Define allowed OG roles required to view a group content.
   *
   * @param int $nid
   *    The node nid.
   * @param array $gids
   *    The associated groups for this node.
   * @param array $rids
   *    The associated og roles required to view this node.
   *
   * @return bool
   *    Return TRUE or FALSE.
   */
  public static function setAllowedOgRole($nid, array $gids, array $rids) {
    $return = TRUE;
    if (!empty($gids)) {
      $transaction = db_transaction();
      try {
        // Delete old values.
        $delete = db_delete('nodeAccess')
          ->condition('nid', $nid);
        $or = db_or();
        foreach ($gids as $gid) {
          $or->condition('realm', OG_CAPR_REALM . ':' . $gid, '=');
        }
        $delete->condition($or)
          ->execute();
        // Add new values.
        if (!empty($rids)) {
          // Insert new values.
          $query = db_insert('nodeAccess')
            ->fields(
              array('nid', 'gid', 'realm', 'grant_view')
            );
          foreach ($gids as $gid) {
            foreach ($rids as $rid) {
              $query->values(
                array(
                  'nid' => $nid,
                  'gid' => $rid,
                  'realm' => OG_CAPR_REALM . ':' . $gid,
                  'grant_view' => 1,
                )
              );
            }
          }
          $query->execute();
        }
      }
      catch (Exception $ex) {
        $transaction->rollback();
        watchdog_exception(__METHOD__, $ex);
        $return = FALSE;
      }
    }
    return $return;
  }
  /**
   * Explain node access for hook_node_access().
   *
   * @param int $gid
   *    Group id.
   * @param int $rid
   *    OG role id.
   *
   * @return string
   *    Description of node access.
   */
  public static function explainNodeAccess($gid, $rid) {
    $role = static::getRidLabel($rid);
    $role = (isset($role->name)) ? $role->name : $role;
    $group = static::getGidLabel($gid);
    $group = (isset($group->title)) ? $group->title : $group;
    return t('User must have %role role in %group group', array('%role' => $role, '%group' => $group));
  }
  /**
   * Get OG role label.
   *
   * @param int $key
   *    The OG role rid.
   *
   * @return string
   *    The OG role label.
   */
  private static function getRidLabel($key = NULL) {
    if (empty(static::$ridLabels)) {
      static::$ridLabels = db_select('og_role', 'R')
        ->fields('R', array('rid', 'name'))
        ->orderBy('R.rid')
        ->execute()
        ->fetchAllAssoc('rid');
    }
    if (!is_null($key)) {
      if (isset(static::$ridLabels[$key])) {
        return static::$ridLabels[$key];
      }
      return t('Undefined');
    }
    else {
      return static::$ridLabels;
    }
  }
  /**
   * Get OG label.
   *
   * @param int $key
   *    The OG nid.
   *
   * @return string
   *    The OG node title.
   */
  private static function getGidLabel($key = NULL) {
    if (is_array(static::$gidLabels) && empty(static::$gidLabels)) {
      if (!field_info_field(OG_GROUP_FIELD)) {
        static::$gid_label = t('Undefined');
      }
      // Get all groups.
      $query = new EntityFieldQuery();
      $groups = $query
        ->entityCondition('entity_type', 'node')
        ->fieldCondition(OG_GROUP_FIELD, 'value', 1, '=')
        ->execute();
      if (isset($groups['node'])) {
        $groups = array_keys($groups['node']);
        static::$gidLabels = db_select('node', 'N')
          ->fields('N', array('nid', 'title'))
          ->condition('N.nid', $groups, 'IN')
          ->orderBy('N.nid')
          ->execute()
          ->fetchAllAssoc('nid');
      }
      else {
        static::$gid_label = t('Undefined');
      }
    }
    if (is_array(static::$gidLabels)) {
      if (!is_null($key)) {
        if (isset(static::$gidLabels[$key])) {
          return static::$gidLabels[$key];
        }
      }
      else {
        return static::$gidLabels;
      }
    }
    return t('Undefined');
  }
  /**
   * Check if defined node have a private access.
   *
   * @param object $node
   *    The node object.
   * @param bool $check_grant
   *    Check if og_capr access is defined.
   *
   * @return bool
   *    Return TRUE or FALSE.
   */
  public static function isPrivateGroupContent($node, $check_grant = TRUE) {
    // Only on group content.
    if (og_is_group_content_type('node', $node->type)) {
      if (is_null(static::$nodeAccess) || (is_array(static::$nodeAccess) && !isset(static::$nodeAccess[$node->nid]))) {
        static::$nodeAccess[$node->nid] = FALSE;
        // Only is isset visibility settings.
        $wrapper = entity_metadata_wrapper('node', $node);
        // If content access is set to private.
        if (!empty($wrapper->{OG_CONTENT_ACCESS_FIELD}) && $wrapper->{OG_CONTENT_ACCESS_FIELD}->value() == OG_CONTENT_ACCESS_PRIVATE) {
          static::$nodeAccess[$node->nid] = TRUE;
        }
        // Otherwise, check default group permission.
        else {
          $gids = array();
          if (module_exists('og_context')) {
            $context = og_context('node');
            if (isset($context['gid'])) {
              $gids[] = $context['gid'];
            }
          }
          if (empty($gids)) {
            $_gids = og_get_entity_groups('node', $node);
            if (isset($_gids['node'])) {
              $gids = $_gids['node'];
            }
          }
          if (!empty($gids)) {
            $groups = node_load_multiple($gids);
            foreach ($groups as $group) {
              $wrapper = entity_metadata_wrapper('node', $group);
              if (!empty($wrapper->{OG_ACCESS_FIELD}) && $wrapper->{OG_ACCESS_FIELD}->value() == 1) {
                static::$nodeAccess[$node->nid] = TRUE;
              }
            }
          }
        }
      }
      if (static::$nodeAccess[$node->nid]) {
        if ($check_grant) {
          return static::isCaprAccessGranted($node);
        }
      }
      return static::$nodeAccess[$node->nid];
    }
    return FALSE;
  }
  /**
   * Check if defined node have a og_capr granted access.
   *
   * @param object $node
   *    The node object.
   *
   * @return bool
   *    Return TRUE or FALSE.
   */
  private static function isCaprAccessGranted($node) {
    $check = db_select('nodeAccess', 'N')
      ->fields('N', array('nid'))
      ->condition('N.nid', $node->nid)
      ->condition('N.realm', OG_CAPR_REALM . ':%', 'like')
      ->range(0, 1)
      ->execute()
      ->fetchObject();
    if (!empty($check)) {
      return TRUE;
    }
    return FALSE;
  }
  /**
   * Check if defined user have access to defined node.
   *
   * @param object $node
   *    The node object.
   * @param int $uid
   *    The user id.
   * @param int $gid
   *    The group id.
   *
   * @return bool
   *    Return TRUE or FALSE.
   */
  public static function checkUserAccess($node, $uid, $gid = NULL) {
    if (is_null($gid)) {
      // Get associated group.
      $gids = og_get_entity_groups('node', $node);
      if (!empty($gids)) {
        $gids = $gids['node'];
      }
    }
    else {
      $gids = (array) $gid;
    }
    if (!empty($gids)) {
      $check = db_select('nodeAccess', 'N');
      $check->innerJoin('og_users_roles', 'R', "N.gid = R.rid");
      $or = db_or();
      foreach ($gids as $gid) {
        $or->condition('N.realm', OG_CAPR_REALM . ':' . $gid, '=');
      }
      $check->condition($or)
          ->condition('R.uid', $uid)
          ->condition('N.nid', $node->nid)
          ->fields('N')
          ->fields('R');
      $results = $check->execute()->fetchAll();
      if (!empty($results)) {
        return TRUE;
      }
    }
    return FALSE;
  }
  /**
   * Return granted OG Capr access.
   *
   * @param object $account
   *    The user account.
   *
   * @return bool
   *    Return TRUE or FALSE.
   */
  public static function getNodeGrants($account) {
    if (is_null(static::$nodeGrants) || (is_array(static::$nodeGrants) && !isset(static::$nodeGrants[$account->uid]))) {
      $results = db_select('og_users_roles', 'R')
        ->fields('R', array('rid', 'gid'))
        ->condition('R.uid', $account->uid)
        ->condition('R.group_type', 'node')
        ->execute()
        ->fetchAll();

      static::$nodeGrants[$account->uid] = array();
      if (!empty($results)) {
        foreach ($results as $item) {
          if (!isset(static::$nodeGrants[$account->uid][OG_CAPR_REALM . ':' . $item->gid])) {
            static::$nodeGrants[$account->uid][OG_CAPR_REALM . ':' . $item->gid] = array();
          }
          static::$nodeGrants[$account->uid][OG_CAPR_REALM . ':' . $item->gid][] = $item->rid;
        }
      }
    }
    return static::$nodeGrants[$account->uid];
  }

}
