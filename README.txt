CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The Organic Groups Content Access Per Role (also referred to as the 'og_capr'
module), provides to users the ability to manage an advanced access control to
private content group.

Each access content can be granted to one or more group roles of current content
group.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/node/2426253


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2426253

REQUIREMENTS
------------
This module requires the following modules:

 * OG Access (https://drupal.org/project/og)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
Group and group content configuration (admin/config/group/fields)
  
 * Add Group visibility field to your group node bundle

 * Add Content group visibility field to your group content node bundle

Content access on OG Roles will be available for :

 * Private content in public group

 * Private content in private group

 * Content with default visibility in private group

You can create default configuration in admin/config/group/permissions

Each group can override default configuration in 
group/node/%node/admin/permissions

Each content group can have his own configuration in node/%node/og_capr

MAINTAINERS
-----------
Current maintainers:
 * Vincent LABUSSIERE (_vl_) - https://www.drupal.org/user/660072
