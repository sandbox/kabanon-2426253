<?php
/**
 * @file
 * Test for OG Capr module.
 */

/**
 * OG Capr test class.
 */
class OgCaprSimplePrivateGroupTest extends OgCaprTest {
  /**
   * Init test informations.
   *
   * @return array
   *    The test informations.
   */
  public static function getInfo() {
    return array(
      'name' => 'OG CAPR Simpe private group',
      'description' => 'Test the content access per role provided by OG CAPR '
      . 'for simple private group (no custom OG role).',
      'group' => 'Organic groups CAPR',
    );
  }
  /**
   * Set up testing environnement.
   */
  protected function setUp() {
    parent::setUp(array(
        // Non group member.
        1,
        // Simple group member (with no group role).
        2,
        // Group administrator.
        4,
        // Site administrator.
        5,
      )
    );
  }
  /**
   * Test access to private group, content group with no content access field.
   */
  public function testPublicGroupNoContentFieldAccess() {
    $group_type = $this->createGroupType();
    $group = $this->createGroupNode($group_type, $this->user[5]->uid, 1);
    // Add membership for users 2 & 4 to this group.
    $this->addUserToGroup($group, $this->user[2], OG_AUTHENTICATED_ROLE);
    $this->addUserToGroup($group, $this->user[4], OG_ADMINISTRATOR_ROLE);
    $group_content_type = $this->createGroupContentType();
    $fields = array(
      OG_AUDIENCE_FIELD => array(
        LANGUAGE_NONE => array(
          array(
            'target_id' => $group->nid,
          ),
        ),
      ),
    );
    $group_content = $this->createGroupContentNode($group_content_type,
      $this->user[5]->uid, $fields);
    $assert = array(
      'view' => array(
        1 => '403',
        2 => '200',
        4 => '200',
        5 => '200',
      ),
      'access capr' => array(
        1 => '403',
        2 => '403',
        4 => '200',
        5 => '200',
      ),
    );
    foreach ($this->user as $key => $user) {
      $this->drupalLogin($this->user[$key]);
      // Assert the user view the node.
      $this->drupalGet('node/' . $group_content->nid);
      $this->assertResponse($assert['view'][$key], 'Correct content group '
        . 'access for user ' . $key . ' with no access content field on '
        . 'private group', 'OG Capr');
      // Assert user access on OG CAPR override configuration page.
      $this->drupalGet('node/' . $group_content->nid . '/og_capr');
      $this->assertResponse($assert['access capr'][$key], 'Correct OG Capr '
        . 'access for user ' . $key . ' on a content group with no access '
        . 'content field set to private on private group', 'OG Capr');
    }
  }
  /**
   * Test access to private group, content access field set to default.
   */
  public function testPublicGroupDefaultContentFieldAccess() {
    $group_type = $this->createGroupType();
    $group = $this->createGroupNode($group_type, $this->user[5]->uid, 1);
    // Add membership for users 2 & 4 to this group.
    $this->addUserToGroup($group, $this->user[2], OG_AUTHENTICATED_ROLE);
    $this->addUserToGroup($group, $this->user[4], OG_ADMINISTRATOR_ROLE);
    $group_content_type = $this->createGroupContentType(TRUE);
    $fields = array(
      OG_AUDIENCE_FIELD => array(
        LANGUAGE_NONE => array(
          array(
            'target_id' => $group->nid,
          ),
        ),
      ),
      OG_CONTENT_ACCESS_FIELD => array(
        LANGUAGE_NONE => array(
          array(
            'value' => OG_CONTENT_ACCESS_DEFAULT,
          ),
        ),
      ),
    );
    $group_content = $this->createGroupContentNode($group_content_type,
      $this->user[5]->uid, $fields);
    $assert = array(
      'view' => array(
        1 => '403',
        2 => '200',
        4 => '200',
        5 => '200',
      ),
      'access capr' => array(
        1 => '403',
        2 => '403',
        4 => '200',
        5 => '200',
      ),
    );
    foreach ($this->user as $key => $user) {
      $this->drupalLogin($this->user[$key]);
      // Assert the user view the node.
      $this->drupalGet('node/' . $group_content->nid);
      $this->assertResponse($assert['view'][$key], 'Correct content group '
        . 'access for user ' . $key . ' with access content field set to '
        . 'default on private group', 'OG Capr');
      // Assert user access on OG CAPR override configuration page.
      $this->drupalGet('node/' . $group_content->nid . '/og_capr');
      $this->assertResponse($assert['access capr'][$key], 'Correct OG Capr '
        . 'access for user ' . $key . ' on a content group with access content '
        . 'field set to default on private group', 'OG Capr');
    }
  }
  /**
   * Test access to private group, content access field set to public.
   */
  public function testPublicGroupPublicContentFieldAccess() {
    $group_type = $this->createGroupType();
    $group = $this->createGroupNode($group_type, $this->user[5]->uid, 1);
    // Add membership for users 2 & 4 to this group.
    $this->addUserToGroup($group, $this->user[2], OG_AUTHENTICATED_ROLE);
    $this->addUserToGroup($group, $this->user[4], OG_ADMINISTRATOR_ROLE);
    $group_content_type = $this->createGroupContentType(TRUE);
    $fields = array(
      OG_AUDIENCE_FIELD => array(
        LANGUAGE_NONE => array(
          array(
            'target_id' => $group->nid,
          ),
        ),
      ),
      OG_CONTENT_ACCESS_FIELD => array(
        LANGUAGE_NONE => array(
          array(
            'value' => OG_CONTENT_ACCESS_PUBLIC,
          ),
        ),
      ),
    );
    $group_content = $this->createGroupContentNode($group_content_type,
      $this->user[5]->uid, $fields);
    $assert = array(
      'view' => array(
        1 => '200',
        2 => '200',
        4 => '200',
        5 => '200',
      ),
      'access capr' => array(
        1 => '403',
        2 => '403',
        4 => '200',
        5 => '200',
      ),
    );
    foreach ($this->user as $key => $user) {
      $this->drupalLogin($this->user[$key]);
      // Assert the user view the node.
      $this->drupalGet('node/' . $group_content->nid);
      $this->assertResponse($assert['view'][$key], 'Correct content group '
        . 'access for user ' . $key . ' with access content field set to '
        . 'public on private group', 'OG Capr');
      // Assert user access on OG CAPR override configuration page.
      $this->drupalGet('node/' . $group_content->nid . '/og_capr');
      $this->assertResponse($assert['access capr'][$key], 'Correct OG Capr '
        . 'access for user ' . $key . ' on a content group with access content '
        . 'field set to public on private group', 'OG Capr');
    }
  }
  /**
   * Test access to private group, content access field set to private.
   */
  public function testPublicGroupPrivateContentFieldAccess() {
    $group_type = $this->createGroupType();
    $group = $this->createGroupNode($group_type, $this->user[5]->uid, 1);
    // Add membership for users 2 & 4 to this group.
    $this->addUserToGroup($group, $this->user[2], OG_AUTHENTICATED_ROLE);
    $this->addUserToGroup($group, $this->user[4], OG_ADMINISTRATOR_ROLE);
    // Create group content type.
    $group_content_type = $this->createGroupContentType(TRUE);
    $fields = array(
      OG_AUDIENCE_FIELD => array(
        LANGUAGE_NONE => array(
          array(
            'target_id' => $group->nid,
          ),
        ),
      ),
      OG_CONTENT_ACCESS_FIELD => array(
        LANGUAGE_NONE => array(
          array(
            'value' => OG_CONTENT_ACCESS_PRIVATE,
          ),
        ),
      ),
    );
    $group_content = $this->createGroupContentNode($group_content_type,
    $this->user[5]->uid, $fields);
    $assert = array(
      'view' => array(
        1 => '403',
        2 => '200',
        4 => '200',
        5 => '200',
      ),
      'access capr' => array(
        1 => '403',
        2 => '403',
        4 => '200',
        5 => '200',
      ),
    );
    foreach ($this->user as $key => $user) {
      $this->drupalLogin($this->user[$key]);
      // Assert the user view the node.
      $this->drupalGet('node/' . $group_content->nid);
      $this->assertResponse($assert['view'][$key], 'Correct content group '
        . 'access for user ' . $key . ' with access content field set to '
        . 'private on private group', 'OG Capr');
      // Assert user access on OG CAPR override configuration page.
      $this->drupalGet('node/' . $group_content->nid . '/og_capr');
      $this->assertResponse($assert['access capr'][$key], 'Correct OG Capr '
        . 'access for user ' . $key . ' on a content group with access content '
        . 'field set to private on private group', 'OG Capr');
    }
  }

}
