<?php
/**
 * @file
 * OG CAPR functions.
 */

/**
 * Form page callback : node/%node/og_capr.
 */
function og_capr_form($form, &$form_state, $node) {
  $gids = og_get_entity_groups('node', $node);
  foreach ($gids['node'] as $gid) {
    // For each associated group, Group visibility have to be private.
    $group = node_load($gid);
    $group_wrapper = entity_metadata_wrapper('node', $group);
    if (!empty($group_wrapper->{OG_ACCESS_FIELD})
      && $group_wrapper->{OG_ACCESS_FIELD}->value() == 1) {
      // For each associated group, user must have og_capr override permission
      // for this content type.
      $perm = 'override default og_capr ' . $node->type . ' permissions';
      if (og_user_access('node', $gid, $perm)) {
        $form['fieldset_' . $gid] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($group->title),
          '#tree' => TRUE,
        );
        // Get all member roles associated to this group and sanitize values.
        $roles = array_map('check_plain', og_roles('node', 'group', $gid, FALSE, FALSE));
        if (!empty($roles)) {
          // Get overrided values.
          $default = OgCapr::getAllowedOgRole($node->nid, (array) $gid);
          $form['fieldset_' . $gid][$gid] = array(
            '#type' => 'checkboxes',
            '#title' => t('Select one or more group roles required in order to view this content.'),
            '#options' => $roles,
            '#default_value' => $default,
          );
        }
        else {
          $form['fieldset_' . $gid]['help_' . $gid] = array(
            '#theme' => 'html_tag',
            '#tag' => 'p',
            '#value' => t('No custom role founded.'),
          );
        }
      }
    }
  }
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
    'cancel' => array(
      '#markup' => l(t('Cancel'), 'node/' . $node->nid),
    ),
  );
  return $form;
}
/**
 * Form page callback : node/%node/og_capr (submit action).
 */
function og_capr_form_submit($form, &$form_state) {
  $return = TRUE;
  foreach ($form_state['values'] as $data) {
    if (is_array($data)) {
      foreach ($data as $gid => $perms) {
        if (!empty($perms)) {
          $rids = array();
          foreach ($perms as $rid) {
            if ($rid) {
              $rids[] = $rid;
            }
          }
          if (!OgCapr::setAllowedOgRole($form_state['values']['nid'], (array) $gid, $rids)) {
            $return = FALSE;
          }
        }
      }
    }
  }
  if ($return) {
    drupal_set_message(t('Content access control successfully updated.'));
  }
  else {
    drupal_set_message(t('An error occurs. See watchdog report more more information.'), 'error');
  }
}
